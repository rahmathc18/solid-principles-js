// Dependency Inversion Principle (DIP):
// DIP: Modul tingkat tinggi tidak bergantung pada modul tingkat rendah
/*Prinsip Inversi Ketergantungan (Dependency Inversion Principle atau DIP) adalah salah satu dari lima prinsip desain dalam SOLID. 
Prinsip ini menyatakan bahwa modul tingkat tinggi tidak boleh bergantung pada modul tingkat rendah, 
melainkan keduanya seharusnya bergantung pada abstraksi.*/

// Kelas Switch yang mengikuti DIP, bergantung pada abstraksi (interface)
class Switch {
    constructor(device) {
        this.device = device;
    }

    toggle() {
        if (this.device.isOn()) {
            this.device.turnOff();
        } else {
            this.device.turnOn();
        }
    }
}

// Kelas Light yang mengikuti DIP, mengimplementasikan abstraksi (interface)
class Light {
    isOn() {
        // Check if light is on
    }

    turnOn() {
        // Turn on the light
    }

    turnOff() {
        // Turn off the light
    }
}
// Penggunaan kelas Switch dan Light yang mengikuti prinsip DIP
const lightSwitch = new Switch(new Light());
lightSwitch.toggle();

// Tidak mengikuti DIP: Ketergantungan langsung pada implementasi rendah

// Kelas Switch yang melanggar DIP dengan ketergantungan langsung pada implementasi rendah
class Switch {
    constructor(light) {
        this.light = light;
    }

    toggle() {
        if (this.light.isOn()) {
            this.light.turnOff();
        } else {
            this.light.turnOn();
        }
    }
}

// Kelas Light yang melanggar DIP dengan tidak menggunakan abstraksi
class Light {
    isOn() {
        return true; // Mendukung keadaan tertentu
    }

    turnOn() {
        // Turn on the light
    }

    turnOff() {
        // Turn off the light
    }
}

// Penggunaan kelas Switch dan Light yang melanggar prinsip DIP
const lightSwitch1 = new Switch(new Light());
lightSwitch1.toggle();

/*Pada implementasi yang mengikuti DIP, kelas Switch dan Light bergantung pada abstraksi, 
yang memungkinkan untuk substitusi komponen dengan mudah. 
Sebaliknya, pada implementasi yang melanggar DIP, kelas 
Switch bergantung secara langsung pada implementasi rendah (Light), yang dapat menyulitkan substitusi dan meningkatkan ketergantungan. Implementasi yang mengikuti DIP mendukung fleksibilitas dan penggunaan prinsip abstraksi untuk mengurangi ketergantungan langsung.*/
