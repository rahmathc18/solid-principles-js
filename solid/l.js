// Liskov Substitution Principle (LSP)
// Objek turunan dapat digunakan sebagai pengganti objek parent

class Bird {
    fly() {
        console.log("Flying");
    }
}

class Sparrow extends Bird {
    sing() {
        console.log("Singing");
    }
}

function makeBirdFly(bird) {
    bird.fly();
}

const bird = new Sparrow();
makeBirdFly(bird);

// Tidak mengikuti LSP: Objek turunan tidak dapat digunakan secara umum

class Bird {
    fly() {
        console.log("Flying");
    }
}

class Penguin extends Bird {
    swim() {
        console.log("Swimming");
    }

    // Tidak menambahkan implementasi fly
}

function makeBirdFly(bird) {
    bird.fly();
}

const penguin = new Penguin();
makeBirdFly(penguin); // Potensial untuk menyebabkan error atau hasil yang tidak diharapkan
