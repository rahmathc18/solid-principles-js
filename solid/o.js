// Open/Closed Principle (OCP):
/*Prinsip Tertutup untuk Modifikasi, 
Terbuka untuk Ekstensi (Open/Closed Principle atau OCP) adalah salah satu dari lima prinsip desain dalam SOLID. 
Prinsip ini menyatakan bahwa kelas-kelas atau modul harus terbuka untuk ekstensi (ditambahkan fungsionalitas baru) tetapi 
tertutup untuk modifikasi (tidak mengubah kode yang sudah ada).*/

// Kelas Shape sebagai kelas dasar yang menyediakan metode area()
class Shape {
    area() {
        throw new Error("Method not implemented");
    }
}

// Kelas Rectangle sebagai ekstensi dari Shape dengan implementasi metode area()
class Rectangle extends Shape {
    constructor(width, height) {
        super();
        this.width = width;
        this.height = height;
    }

    area() {
        return this.width * this.height;
    }
}

// Kelas AreaCalculator untuk menghitung total area dari berbagai bentuk
class AreaCalculator {
    constructor(shapes) {
        this.shapes = shapes;
    }

    calculateTotalArea() {
        return this.shapes.reduce((total, shape) => total + shape.area(), 0);
    }
}

// Kelas Circle sebagai ekstensi dari Shape dengan implementasi metode area()
class Circle extends Shape {
    constructor(radius) {
        super();
        this.radius = radius;
    }

    area() {
        return Math.PI * this.radius ** 2;
    }
}
// Penggunaan kelas-kelas
const shapes = [new Rectangle(4, 5), new Circle(3)];

const areaCalculator = new AreaCalculator(shapes);
console.log("Total Area:", areaCalculator.calculateTotalArea()); // Output: Total Area: (sesuai hasil perhitungan)

// Melanggar OCP: Memodifikasi kelas yang sudah ada
// Kelas Rectangle yang melanggar OCP dengan menambahkan fungsionalitas baru
class Rectangle {
    constructor(width, height) {
        this.width = width;
        this.height = height;
    }

    calculateArea() {
        return this.width * this.height;
    }
}

// Kelas Circle yang merupakan ekstensi dari Rectangle
class Circle extends Rectangle {
    constructor(radius) {
        super(radius, radius);
        this.radius = radius;
    }

    calculateArea() {
        return Math.PI * this.radius ** 2;
    }
}

const rectangle = new Rectangle(4, 5);
console.log("Rectangle Area:", rectangle.calculateArea()); // Output: Rectangle Area: (sesuai hasil perhitungan)

const circle = new Circle(3);
console.log("Circle Area:", circle.calculateArea()); // Output: Circle Area: (sesuai hasil perhitungan)

/*Pada contoh yang mengikuti OCP, kelas Shape, Rectangle, dan Circle 
dapat diubah dengan menambahkan fungsionalitas baru tanpa mengubah AreaCalculator. 
Sebaliknya, pada contoh yang melanggar OCP, kelas Rectangle mengalami modifikasi untuk menambahkan fungsionalitas baru, yang dapat merusak prinsip OCP. 
Implementasi yang mengikuti OCP memungkinkan fleksibilitas dan perluasan kode tanpa mengubah kode yang sudah ada.*/
