// Single Responsibility Principle (SRP): satu tanggung jawab per kelas
/* Prinsip Satu Tanggung Jawab (Single Responsibility Principle atau SRP) 
adalah salah satu dari lima prinsip desain dalam SOLID.
Prinsip ini menyatakan bahwa suatu kelas seharusnya hanya memiliki satu alasan untuk berubah atau,
dengan kata lain, hanya satu tanggung jawab.*/

// Kelas Calculator dengan satu tanggung jawab yaitu operasi matematika (penjumlahan)
class Calculator {
    // Metode sum untuk melakukan penjumlahan dua angka
    sum(a, b) {
        return a + b;
    }
}

// Kelas Logger dengan satu tanggung jawab yaitu menangani logging
class Logger {
    // Metode log untuk mencetak pesan log ke konsol
    log(message) {
        console.log(message);
    }
}

// Penggunaan kelas Calculator untuk melakukan penjumlahan
const calculator = new Calculator();
const result = calculator.sum(3, 5);

// Penggunaan kelas Logger untuk mencetak hasil ke konsol
const logger = new Logger();
logger.log(`Result: ${result}`); // Output: Result: 8

// Tidak mengikuti SRP: Satu kelas melakukan terlalu banyak hal
class User {
    //note : method constructor untuk menginitialize/membuat properti value
    constructor(name) {
        //note : this merujuk pada classnya sendiri
        this.name = name;
    }
    // Metode untuk mencetak log saat pengguna masuk
    logIn() {
        console.log(`${this.name} logged in.`);
    }

    // Metode untuk mengirim email kepada pengguna
    sendEmail() {
        console.log(`Email sent to ${this.name}.`);
    }
}

// Penggunaan kelas User dengan dua tanggung jawab yang berbeda
const user = new User("Si Lorem"); // Menginiatialize class user
user.logIn(); // Output: Si Lorem logged in
user.sendEmail(); // Output: Email sent to Si Lorem

/*Pada contoh yang tidak mengikuti SRP, 
kelas User memiliki dua tanggung jawab yang berbeda, 
yaitu mengelola log masuk dan mengirim email. 
Hal ini melanggar prinsip SRP karena satu kelas seharusnya hanya memiliki satu alasan untuk berubah. 
Pemisahan antara tanggung jawab logika bisnis dan tanggung jawab logging 
dapat meningkatkan fleksibilitas dan pemeliharaan kode.*/
