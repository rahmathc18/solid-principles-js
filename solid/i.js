// Interface Segregation Principle (ISP)
/*Prinsip Pensegregasian Antarmuka (Interface Segregation Principle atau ISP) 
adalah salah satu dari lima prinsip desain dalam SOLID. 
Prinsip ini menyatakan bahwa sebaiknya sebuah kelas tidak dipaksa 
untuk mengimplementasikan metode yang tidak relevan atau tidak dibutuhkan.*/

class Workable {
    work() {
        throw new Error("Method not implemented");
    }
}

class Eatable {
    eat() {
        throw new Error("Method not implemented");
    }
}
// kelas worker merupakan turunan dari class Work
class Worker extends Workable {
    work() {
        console.log("Working");
    }
}

class Human extends Worker {
    constructor() {
        //super untuk memanggil properti atau method yang ada pada parent class
        super();
        this.eatable = new EatableImpl();
    }

    eat() {
        this.eatable.eat();
    }
}

class EatableImpl extends Eatable {
    eat() {
        console.log("Eating");
    }
}

const worker = new Worker();
worker.work();

const human = new Human();
human.work();
human.eat();

// Melanggar ISP: Antarmuka yang menyertakan metode tidak relevan

class Workable {
    work() {
        throw new Error("Method not implemented");
    }
}

class Eatable {
    eat() {
        throw new Error("Method not implemented");
    }
}

class Worker extends Workable {
    work() {
        console.log("Working");
    }
}

class Human extends Worker {
    constructor() {
        super();
        this.eatable = new EatableImpl();
    }

    eat() {
        this.eatable.eat();
    }
}

class EatableImpl extends Eatable {
    eat() {
        console.log("Eating");
    }
}

const workerObj = new Worker();
workerObj.work();

const humanObj = new Human();
humanObj.work();
humanObj.eat();

/*Pada implementasi yang mengikuti ISP, setiap antarmuka memiliki tanggung jawab yang sesuai, 
dan kelas-kelas yang mengimplementasikannya hanya menyediakan metode yang relevan. 
Sebaliknya, pada implementasi yang melanggar ISP, terdapat antarmuka yang menyertakan metode yang tidak relevan, dan hal ini dapat menyebabkan kelas yang mengimplementasikannya mempunyai metode yang tidak diperlukan. 
Implementasi yang mengikuti ISP meningkatkan fleksibilitas dan pemeliharaan kode.*/
